tabularfile
============

Tabular files for humans or an opinionated approach to tabular files or
whatever. Parse and write ODS files, mimicing the `csv` module interface,
keeping memory usage as low as possible.

Reading
-------

The main API is `tabularfile.load(path_or_file, **kwarsg)` it's a context manager returning an
iterable. It accepts as first argument a bytes string, a path or an opened
file. Other arguments depends on the backend, ods or csv.

.. code:: pycon

    >>> from tabularfile import load
    >>> with load('sheet.ods') as tabfile:
        list(tabfile)
    [
        ['date', 'count'],
        ['01/12/2019', '123'],
        ['01/01/2020', '156'],
    ]
    >>> with load('sheet.csv') as tabfile:
        list(tabfile)
    [
        ['date', 'count'],
        ['01/12/2019', '123'],
        ['01/01/2020', '156'],
    ]


With `typed=True` you can ask the reader to cast cells content based on the declared OpenDocument value type.


.. code:: pycon

    >>> with load('sheet.ods'), typed=True) as tabfile:
            list(tabfile)
    [
        ['date', 'count']
        [datetime.date(2019, 12, 1), 123],
        [datetime.date(2029, 1, 1), 156],
    ]


With the `sheet` constructor attribute you can load another sheet than the
first one, only integer indexes are supported, it also supports sheet's name.
To get the list of tsheets you can use the `tabfile.sheets` accessor.


.. code:: pycon

    >>> with load('sheet.ods', sheet=1) as tabfile:
    ...
    >>> with load('sheet.ods', sheet='Sheet1') as tabfile:
            tabfile.sheets
    ['Sheet1', 'Sheet2']

Writing
-------

To write a sheet file, use the `tabularfile.write(path_or_file, format='ods',
**kwargs)` context manager. `format` can also be `csv` and in this case it
accepts other arguments like `encoding`, `dialect`, `delimiter` or `quotechar`.

The ODS writer accept special value `tabularfile.ods.LinkedValue` if you need
to put XLink on your data. ODS and CSV writer accets date and datetime values
which will be formatted using the `date_format` and `datetime_format`
templates.


.. code:: pycon

    >>> from tabularfile import ods
    >>> with ods.writer('sheet.ods') as writer:
        writer.writerow(['date', 'count', 'link'])
        writer.writerows([
            [datetime.date(2019, 12, 1), 123, ods.LinkedValue('Click me', href='https://example.com/')],
            [datetime.date(2020, 1, 1), 156, ods.LinkedValue('Click me', href='https://example.com/')],
        ])

Parsing ISO8601 dates
---------------------

Base python before version 3.7 is not able to parse date with timezone, so we
try as much as possible to use other library to do it. `isodate` or
`python-dateutil` are used if present.

Detecting CSV character encoding
--------------------------------

If the charamel_ package is installed, it is used for detecting the encoding of CSV files.


.. _charamel: https://pypi.org/project/charamel/
