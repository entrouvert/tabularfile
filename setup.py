# tabularfile
# Copyright (C) 2020 Entr'ouvert

import os
import subprocess

from distutils.command.sdist import sdist
import setuptools


def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
       tag exists, take 0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        with open('VERSION', 'w') as version_file:
            version_file.write(version)
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


cmdclass = {'sdist': eo_sdist}

with open('README', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='tabularfile',
    version=get_version(),
    author='Benjamin Dauvergne',
    author_email='bdauvergne@entrouvert.com',
    description='A small example package',
    long_description=long_description,
    long_description_content_type='text/x-rst',
    url='https://dev.entrouvert.org/projects/tabularfile/',
    packages=setuptools.find_packages(exclude=['tests']),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.5',
    install_requires=['lxml'],
    cmdclass=cmdclass,
)
