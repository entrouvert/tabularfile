# tabularfile.common- simple ods reader and writer
# Copyright (C) 2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

try:
    import isodate
except ImportError:
    isodate = False

try:
    import dateutil.parser as dateutil_parser
except ImportError:
    dateutil_parser = None


class TabularFileError(Exception):
    pass


def parse_datetime(value):
    if 'T' not in value:
        raise ValueError
    if isodate:
        return isodate.parse_datetime(value)
    if dateutil_parser:
        if hasattr(dateutil_parser, 'isoparse'):
            return dateutil_parser.isoparse(value)
        else:
            return dateutil_parser.parse(value)
    if hasattr(datetime.datetime, 'fromisoformat'):
        return datetime.datetime.fromisoformat(value)
    return datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%S')


def parse_date(value):
    if isodate:
        return isodate.parse_date(value)
    if dateutil_parser:
        if hasattr(dateutil_parser, 'isoparse'):
            return dateutil_parser.isoparse(value).date()
        else:
            return dateutil_parser.parse(value).date()
    if hasattr(datetime.date, 'fromisoformat'):
        return datetime.date.fromisoformat(value)
    return datetime.datetime.strptime(value, '%Y-%m-%d').date()
