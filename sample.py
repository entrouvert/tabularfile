import datetime

from tabularfile import write

with write('sheet.ods', date_format='%d/%m/%Y', datetime_format='%d/%m/%Y %H:%M:%S') as writer:
    writer.writerow(['date', 'count'])
    writer.writerows([
        [datetime.date(2019, 12, 1), 123],
        [datetime.date(2020, 1, 1), 156],
        [datetime.datetime(2020, 1, 1, 12, 0, 0), 156],
    ])
    with writer.cell_writer as cell_writer:
        cell_writer('coucou', href='https://example.com/')
        cell_writer('coucou', href='https://example.com/a/')
